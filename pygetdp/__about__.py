# -*- coding: utf-8 -*-
#

from datetime import date

__version__ = "1.0.0"
__author__ = "Benjamin Vial"
__author_email__ = "benjamin.vial84@gmail.com"
__copyright__ = "Copyright (c) 2018-{}, {} <{}>".format(
    date.today().year, __author__, __author_email__
)
__website__ = "https://gitlab.com/benvial/pygetdp"
__webdoc__ = "https://benvial.gitlab.io/pygetdp/index.html"
__license__ = "License :: OSI Approved :: MIT License"
__status__ = "Development Status :: 5 - Production/Stable"
__description__ = "Python frontend for GetDP."
# Development Status :: 1 - Planning
# Development Status :: 2 - Pre-Alpha
# Development Status :: 3 - Alpha
# Development Status :: 4 - Beta
# Development Status :: 5 - Production/Stable
# Development Status :: 6 - Mature
# Development Status :: 7 - Inactive
