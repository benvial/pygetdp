
:orphan:

.. _sphx_glr_sg_execution_times:


Computation times
=================
**00:05.956** total execution time for 6 files **from all galleries**:

.. container::

  .. raw:: html

    <style scoped>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.13.6/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
    </style>
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap5.min.js"></script>
    <script type="text/javascript" class="init">
    $(document).ready( function () {
        $('table.sg-datatable').DataTable({order: [[1, 'desc']]});
    } );
    </script>

  .. list-table::
   :header-rows: 1
   :class: table table-striped sg-datatable

   * - Example
     - Time
     - Mem (MB)
   * - :ref:`sphx_glr_auto_examples_functionspace_plot_functionspace.py` (``../examples/functionspace/plot_functionspace.py``)
     - 00:01.049
     - 0.0
   * - :ref:`sphx_glr_auto_examples_constraint_plot_constraint.py` (``../examples/constraint/plot_constraint.py``)
     - 00:01.043
     - 0.0
   * - :ref:`sphx_glr_auto_examples_integration_plot_integration.py` (``../examples/integration/plot_integration.py``)
     - 00:01.029
     - 0.0
   * - :ref:`sphx_glr_auto_examples_jacobian_plot_jacobian.py` (``../examples/jacobian/plot_jacobian.py``)
     - 00:00.967
     - 0.0
   * - :ref:`sphx_glr_auto_examples_group_plot_group.py` (``../examples/group/plot_group.py``)
     - 00:00.942
     - 0.0
   * - :ref:`sphx_glr_auto_examples_function_plot_function.py` (``../examples/function/plot_function.py``)
     - 00:00.925
     - 0.0
