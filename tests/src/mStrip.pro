// This code was created by pygetdp v1.0.0.

/*       File 'mStrip.pro'      This file defines the problem dependent
data structures for the     microstrip problem.      To compute the
solution:         getdp mStrip -solve EleSta_v      To compute post-
results:         getdp mStrip -pos Map         or getdp mStrip -pos
Cut */
Group{
    Air = Region[ 101 ];
    Diel1 = Region[ 111 ];
    Ground = Region[ 120 ];
    Line = Region[ 121 ];
    SurfInf = Region[ 130 ];
    DomainCC_Ele = Region[ {Air, Diel1} ];
    DirichletSurf = Region[ {Ground, SurfInf} ];
    }

Function{
    epsr[Air] = 1.0 ;
    epsr[Diel1] = 9.8 ;
    eps0 = 8.854187818e-12 ;
    e = 1e-07 ;
    }

Constraint{
    { Name ElectricScalarPotential; Type Assign; 
         Case  {  
           { Region DirichletSurf; Value 0; } 
           { Region Line; Value 0.001; } 
         }
    }
    }

FunctionSpace{
       { Name Hgrad_v_Ele; Type Form0; 
           BasisFunction {  
            {  Name sn; NameOfCoef vn; Function BF_Node; Support DomainCC_Ele; Entity NodesOf[All]; } 
            }
           Constraint {  
            {  NameOfCoef vn; EntityType NodesOf; NameOfConstraint ElectricScalarPotential; } 
            }
       }
    }

Jacobian{
    { Name Vol; 
         Case  {  
           { Region All; Jacobian Vol; } 
         }
    }
    }

Integration{
    { Name GradGrad; 
         Case {  
         {  Type Gauss ; 
         Case  { 
            {  GeoElement Triangle; NumberOfPoints 4; } 
            {  GeoElement Quadrangle; NumberOfPoints 4; } 
            {  GeoElement Tetrahedron; NumberOfPoints 4; } 
            {  GeoElement Hexahedron; NumberOfPoints 6; } 
            {  GeoElement Prism; NumberOfPoints 9; }  
          }
    }
    }
    }
    }

Formulation{
    { Name Electrostatics_v; Type FemEquation; 
         Quantity {  
            {  Name v; Type Local; NameOfSpace Hgrad_v_Ele; } 
            }
         Equation {  
           Galerkin { [ epsr[] * Dof{d v} , {d v} ]; In DomainCC_Ele; Jacobian Vol; Integration GradGrad; } 
            }
    }
    }

Resolution{
    { Name EleSta_v; 
         System {  
            { Name Sys_Ele; NameOfFormulation Electrostatics_v; } 
            }
         Operation {  
           Generate[Sys_Ele]; Solve[Sys_Ele]; SaveSolution[Sys_Ele];  }
    }
    }

PostProcessing{
    { Name EleSta_v; NameOfFormulation Electrostatics_v; 
         Quantity {  
            {  Name v; Value  {  Local {  [ {v} ]; In DomainCC_Ele; Jacobian Vol; } } }
            {  Name e; Value  {  Local {  [ -{d v} ]; In DomainCC_Ele; Jacobian Vol; } } }
            {  Name d; Value  {  Local {  [ -eps0*epsr[] * {d v} ]; In DomainCC_Ele; Jacobian Vol; } } }
            }
    }
    }

PostOperation{
    { Name Map; NameOfPostProcessing EleSta_v; 
         Operation {  
            Print [ v, OnElementsOf DomainCC_Ele, File "mStrip_v.pos" ]; 
            Print [ e, OnElementsOf DomainCC_Ele, File "mStrip_e.pos" ]; 
            }
    }
    { Name Cut; NameOfPostProcessing EleSta_v; 
         Operation {  
            Print [ e, OnLine {{e,e,0}{10.e-3,e,0}} {500}, File "Cut_e" ]; 
            }
    }
    }
