#!/usr/bin/python
# -*- coding: utf-8 -*-
#


from pygetdp import render
from pygetdp.functionspace import FunctionSpace


def test_constraint():
    fs = FunctionSpace()
    hgrad = fs.add("Hgrad")

    hgrad.add_basis_function(
        Name="sn",
        NameOfCoef="vn",
        Function="BF_Node",
        Support="Domain",
        Entity="NodesOf[All]",
    )
    hgrad.add_basis_function(
        Name="s2",
        NameOfCoef="v2",
        Function="BF_Node_2E",
        Support="Domain",
        Entity="EdgesOf[All]",
    )
    hgrad.add_global_quantity(
        Name="GlobalElectricPotential", Type="AliasOf", NameOfCoef="vf"
    )
    hgrad.add_global_quantity(
        Name="GlobalElectricCharge", Type="AssociatedWith", NameOfCoef="Bloch"
    )

    hgrad.add_constraint(
        NameOfCoef="vn", EntityType="NodesOf", NameOfConstraint="Dirichlet"
    )
    hgrad.add_constraint(
        NameOfCoef="vn", EntityType="NodesOf", NameOfConstraint="Bloch"
    )
    #
    print("-" * 37)
    render(fs.code)
