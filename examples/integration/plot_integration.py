# -*- coding: utf-8 -*-
"""
Integration
==============

An example to define a Integration object
"""

from pygetdp import Integration
from pygetdp.helpers import build_example_png, print_html

##############################################################################
# A commonly used numerical integration method is the Gauss integration, with a
# number of integration points (``NumberOfPoints``) depending on geometrical
# element types (``GeoElement``), i.e.


integ = Integration()
Int1 = integ.add("Int1")
case0 = Int1.add()  # add a case, this is appended to Int1.cases
assert Int1.cases[0] == case0
item0 = case0.add(Type="Gauss")  # add an item to this case, appended to case0.items
item0.add()  # add a case, this is appended to item0.cases
i = item0.cases[0]
i.add(GeoElement="Triangle", NumberOfPoints=4)
i.add(GeoElement="Quadrangle ", NumberOfPoints=4)
i.add(GeoElement="Tetrahedron", NumberOfPoints=4)
i.add(GeoElement="Hexahedron ", NumberOfPoints=6)
i.add(GeoElement="Prism", NumberOfPoints=9)

print_html(integ.code, tmp_file="out0.html")

####################################
# .. raw:: html
#     :file: out0.html


##############################################################################
# The method above is valid for both 2D and 3D problems,
# for different kinds of elements.


##############################################################################
# Finally we write and render the code as png

build_example_png(integ.code)
