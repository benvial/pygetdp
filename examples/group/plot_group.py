# -*- coding: utf-8 -*-
"""
Group
==========================================

An example to define a Group object
"""

from pygetdp import Group
from pygetdp.helpers import build_example_png, print_html

##############################################################################
# Instanciate the :py:class:`Group` object

group = Group()
##############################################################################
# Let us assume that some elements in the input mesh have the region numbers
# 1000, 2000 and 3000. In the definitions

Air = group.add("Air", 1000)
Core = group.add(id="Core", glist=[2000])
Inductor = group.Region(id="Inductor", glist=[3000], comment="Inductor")
NonConductingDomain = group.Region("NonConductingDomain", [Air, Core])
ConductingDomain = group.Region("ConductingDomain", Inductor)


##############################################################################
# ``Air``, ``Core``, ``Inductor`` are identifiers of elementary region groups while
# NonConductingDomain and ConductingDomain are global region groups.
# Note that the add method defines a ``Region`` group type by default.
#
#
# Groups of function type contain lists of entities built on the
# region groups appearing in their arguments. For example,

Gnode = group.NodesOf("Gnode", glist="NonConductingDomain")

##############################################################################
# represents the group of nodes of geometrical elements belonging to the
# regions in ``NonConductingDomain`` and

Gedges = group.EdgesOf("Gedges", glist="DomainC", Not="SkinDomainC")

##############################################################################
# represents the group of edges of geometrical elements belonging to the
# regions in ``DomainC`` but not to those of ``SkinDomainC``.

print(group.code)

####################################
# Here is the html rendering:
print_html(group.code)


####################################
# .. raw:: html
#     :file: out.html

##############################################################################
# Finally we write and render the code as png

build_example_png(group.code)
